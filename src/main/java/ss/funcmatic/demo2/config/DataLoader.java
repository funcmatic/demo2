package ss.funcmatic.demo2.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import ss.funcmatic.demo2.entity.Category;
import ss.funcmatic.demo2.entity.Role;
import ss.funcmatic.demo2.entity.User;
import ss.funcmatic.demo2.repo.CategoryRepository;
import ss.funcmatic.demo2.repo.UserRepository;

import java.util.*;

@Component
public class DataLoader implements ApplicationRunner {

    private static final Logger log = LoggerFactory.getLogger(DataLoader.class);

    private CategoryRepository categoryRepository;
    private UserRepository userRepository;

    public DataLoader(CategoryRepository categoryRepository, UserRepository userRepository) {
        this.categoryRepository = categoryRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        if (userRepository.findAll().isEmpty()) {
            addInitialUsers();
        }
        if (categoryRepository.findAll().isEmpty()) {
            addInitialCategories();
        }
        log.info(": : : :\tDONE");
    }

    private void addInitialUsers() {
        User admin = new User();
        admin.setUsername("admin");
        admin.setPassword("admin");
        admin.setActive(true);
        Set<Role> roleSet = new HashSet<>();
        roleSet.add(Role.ADMIN);
        roleSet.add(Role.MANAGER);
        admin.setRoles(roleSet);
        userRepository.save(admin);
        log.info(": : : :\tInitial users created");
    }

    private void addInitialCategories() {
        Category noneCategory = new Category();
        noneCategory.setName("none");
        noneCategory.setDescription("no description");
        categoryRepository.save(noneCategory);
        noneCategory.setParentCategory(noneCategory);
        categoryRepository.save(noneCategory);
        log.info(": : : :\tInitial categories created");
    }
}
