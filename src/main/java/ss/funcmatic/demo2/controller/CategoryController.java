package ss.funcmatic.demo2.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import ss.funcmatic.demo2.entity.Category;
import ss.funcmatic.demo2.entity.validator.CategoryValidator;
import ss.funcmatic.demo2.repo.CategoryRepository;

import javax.validation.Valid;

@Controller
@RequestMapping("/category")
@PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
public class CategoryController {

    private static final String VIEW_CATEGORY_EDIT_FORM = "/category/editCategory";
    private static final String VIEW_CATEGORY_LIST = "/category/categoryList";

    private CategoryRepository categoryRepository;
    private CategoryValidator categoryValidator;

    public CategoryController(CategoryRepository categoryRepository, CategoryValidator categoryValidator) {
        this.categoryRepository = categoryRepository;
        this.categoryValidator = categoryValidator;
    }

    @InitBinder("category")
    public void initCategoryBinder(WebDataBinder dataBinder) {
        dataBinder.setValidator(categoryValidator);
    }

    @GetMapping
    public String categoryList(Model model) {
        model.addAttribute("categories", categoryRepository.findAll());
        return VIEW_CATEGORY_LIST;
    }

    @GetMapping("new")
    public String categoryEditForm(Model model) {
        model.addAttribute("categories", categoryRepository.findAll());
        Category category = new Category();
        model.addAttribute("category", category);
        return VIEW_CATEGORY_EDIT_FORM;
    }

    @GetMapping("{category}")
    public String categoryEditForm(@PathVariable Category category, Model model) {
        model.addAttribute("categories", categoryRepository.findAll());
        model.addAttribute("category", category);
        return VIEW_CATEGORY_EDIT_FORM;
    }

    @PostMapping
    public String editCategory(@Valid Category category, BindingResult bindingResult, Model model) {
        String view;

        if (!bindingResult.hasErrors()) {
            String message = category.getName() + (category.isNew() ? " created" : " updated");
            categoryRepository.save(category);
            model.addAttribute("message", message);
            view = VIEW_CATEGORY_LIST;
        } else {
            model.addAttribute("category", category);
            view = VIEW_CATEGORY_EDIT_FORM;
        }
        model.addAttribute("categories", categoryRepository.findAll());
        return view;
    }
}
