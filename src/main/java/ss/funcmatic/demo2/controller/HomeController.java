package ss.funcmatic.demo2.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ss.funcmatic.demo2.entity.Product;
import ss.funcmatic.demo2.entity.User;
import ss.funcmatic.demo2.repo.ProductRepository;
import ss.funcmatic.demo2.repo.UserRepository;

@Controller
public class HomeController {

    private UserRepository userRepository;
    private ProductRepository productRepository;

    public HomeController(UserRepository userRepository, ProductRepository productRepository) {
        this.userRepository = userRepository;
        this.productRepository = productRepository;
    }

    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute("products", productRepository.findAll());
        return "/browse/productList";
    }

    @GetMapping("/contacts")
    public String contacts(Model model) {
        return "contacts";
    }

    //TODO rework product buying process
    @GetMapping("browse/{product_id}/buy")
    @PreAuthorize("hasAuthority('USER')")
    public String buyProduct(@PathVariable("product_id") Integer productId,
                             @AuthenticationPrincipal User user, final Model model) {

        user = userRepository.findById(user.getId()).get();
        Product product = productRepository.findById(productId).get();
        user.getProducts().add(product);
        product.getUsers().add(user);

        userRepository.save(user);

        String message = "Congratulations " + user.getUsername()
                + "! you now own " + product.getName();

        model.addAttribute("message", message);
        return home(model);
    }

    @GetMapping("/browse/owned")
    @PreAuthorize("hasAuthority('USER')")
    public String ownedProducts(@AuthenticationPrincipal User user, final Model model) {

        user = userRepository.findById(user.getId()).get();

        model.addAttribute("products", user.getProducts());
        return "/browse/ownedList";
    }

}
