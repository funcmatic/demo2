package ss.funcmatic.demo2.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import ss.funcmatic.demo2.entity.Product;
import ss.funcmatic.demo2.entity.validator.ProductValidator;
import ss.funcmatic.demo2.repo.CategoryRepository;
import ss.funcmatic.demo2.repo.ProductRepository;
import ss.funcmatic.demo2.repo.UserRepository;

import javax.validation.Valid;

@Controller
@RequestMapping("/product")
@PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
public class ProductController {
    private static final String VIEW_PRODUCT_EDIT_FORM = "/product/editProduct";
    private static final String VIEW_PRODUCT_LIST = "/product/productList";

    private ProductRepository productRepository;
    private UserRepository userRepository;
    private CategoryRepository categoryRepository;
    private ProductValidator productValidator;

    public ProductController(ProductRepository productRepository,
                             UserRepository userRepository,
                             CategoryRepository categoryRepository,
                             ProductValidator productValidator) {
        this.productRepository = productRepository;
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
        this.productValidator = productValidator;
    }

    @InitBinder("product")
    public void initProductBinder(WebDataBinder dataBinder) {
        dataBinder.setValidator(productValidator);
    }

    @GetMapping
    public String productList(Model model) {
        model.addAttribute("products", productRepository.findAll());
        return VIEW_PRODUCT_LIST;
    }

    @GetMapping("new")
    public String createProductFrom(Model model) {
        model.addAttribute("categories", categoryRepository.findAll());
        model.addAttribute("product", new Product());
        return VIEW_PRODUCT_EDIT_FORM;
    }

    @GetMapping("{product}")
    public String createProductFrom(@PathVariable Product product, Model model) {
        model.addAttribute("categories", categoryRepository.findAll());
        model.addAttribute("product", product);
        return VIEW_PRODUCT_EDIT_FORM;
    }

    @PostMapping
    public String editProduct(@Valid Product product, BindingResult bindingResult, Model model) {

        if (!bindingResult.hasErrors()) {
            String message = product.getName() + (product.isNew() ? " created" : " updated");
            productRepository.save(product);
            model.addAttribute("message", message);
            model.addAttribute("products", productRepository.findAll());
            return VIEW_PRODUCT_LIST;
        }
        model.addAttribute("product", product);
        model.addAttribute("categories", categoryRepository.findAll());
        return VIEW_PRODUCT_EDIT_FORM;
    }
}
