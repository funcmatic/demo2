package ss.funcmatic.demo2.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import ss.funcmatic.demo2.entity.Role;
import ss.funcmatic.demo2.entity.User;
import ss.funcmatic.demo2.entity.validator.UserValidator;
import ss.funcmatic.demo2.repo.UserRepository;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

@Controller
public class RegController {
    public static final String VIEW_LOGIN_FORM = "login";
    public static final String VIEW_REG_FORM = "reg";

    private UserRepository userRepository;
    private UserValidator userValidator;

    public RegController(UserRepository userRepository, UserValidator userValidator) {
        this.userRepository = userRepository;
        this.userValidator = userValidator;
    }

    @InitBinder("user")
    public void initCategoryBinder(WebDataBinder dataBinder) {
        dataBinder.setValidator(userValidator);
    }

    @GetMapping("/login")
    public String login() {
        return VIEW_LOGIN_FORM;
    }

    @GetMapping("/reg")
    public String getRegPage(Model model) {
        model.addAttribute("user", new User());
        return VIEW_REG_FORM;
    }

    @PostMapping("/reg")
    public String regNewUser(@Valid User user, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("message", "Error!");
            model.addAttribute("user", user);
            return VIEW_REG_FORM;

        } else if (userRepository.findByUsername(user.getUsername()).isPresent()) {
            model.addAttribute("message", "User exists!");
            model.addAttribute("user", user);
            return VIEW_REG_FORM;
        }
        Set<Role> roleSet = new HashSet<>();
        roleSet.add(Role.USER);
        user.setRoles(roleSet);
        user.setActive(true);
        userRepository.save(user);

        return "redirect:/" + VIEW_LOGIN_FORM;
    }
}
