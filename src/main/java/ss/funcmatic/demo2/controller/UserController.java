package ss.funcmatic.demo2.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import ss.funcmatic.demo2.entity.Role;
import ss.funcmatic.demo2.entity.User;
import ss.funcmatic.demo2.entity.validator.UserValidator;
import ss.funcmatic.demo2.repo.UserRepository;

import javax.validation.Valid;

@Controller
@RequestMapping("/user")
@PreAuthorize("hasAuthority('ADMIN')")
public class UserController {
    public static final String VIEW_USER_EDIT_FORM = "user/editUser";
    public static final String VIEW_USER_LIST = "user/userList";

    private UserRepository userRepository;
    private UserValidator userValidator;

    public UserController(UserRepository userRepository, UserValidator userValidator) {
        this.userRepository = userRepository;
        this.userValidator = userValidator;
    }

    @InitBinder("user")
    public void initCategoryBinder(WebDataBinder dataBinder) {
        dataBinder.setValidator(userValidator);
    }

    @GetMapping
    public String userList(Model model) {
        model.addAttribute("users", userRepository.findAll());
        return VIEW_USER_LIST;
    }

    @GetMapping("{user}")
    public String userEditForm(@PathVariable User user, Model model) {
        model.addAttribute("user", user);
        model.addAttribute("roles", Role.values());
        return VIEW_USER_EDIT_FORM;
    }

    @PostMapping
    public String editUser(@Valid User user, BindingResult bindingResult, final Model model) {
        if (!bindingResult.hasErrors()) {
            userRepository.save(user);
            model.addAttribute("users", userRepository.findAll());
            model.addAttribute("message", user.getUsername() + " updated");
            return VIEW_USER_LIST;
        }
        model.addAttribute("user", user);
        model.addAttribute("roles", Role.values());
        return VIEW_USER_EDIT_FORM;
    }
}
