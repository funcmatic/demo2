package ss.funcmatic.demo2.entity.validator;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ss.funcmatic.demo2.entity.Category;

@Component
public class CategoryValidator implements Validator {
    private static final String REQUIRED = "required";
    private static final String INVALID = "invalid";

    @Override
    public boolean supports(Class<?> clazz) {
        return Category.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Category category = (Category) target;

        // name validation
        String name = category.getName();
        if (!StringUtils.hasLength(name)) {
            errors.rejectValue("name", REQUIRED, REQUIRED);
        } else if (name.length() > 50) {
            errors.rejectValue("name", INVALID, "name length should be less than 50");
        }

        // description validation
        if (!StringUtils.hasLength(category.getDescription())) {
            errors.rejectValue("description", REQUIRED, REQUIRED);
        }

        // parent category validation
        if (category.getParentCategory() == null) {
            errors.rejectValue("parentCategory", REQUIRED, REQUIRED);
        }
    }
}
