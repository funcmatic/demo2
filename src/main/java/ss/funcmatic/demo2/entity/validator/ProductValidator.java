package ss.funcmatic.demo2.entity.validator;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ss.funcmatic.demo2.entity.Category;
import ss.funcmatic.demo2.entity.Product;

@Component
public class ProductValidator implements Validator {
    private static final String REQUIRED = "required";
    private static final String INVALID = "invalid";

    @Override
    public boolean supports(Class<?> clazz) {
        return Product.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Product product = (Product) target;

        // brand validation
        String brand = product.getBrand();
        if (!StringUtils.hasLength(brand)) {
            errors.rejectValue("brand", REQUIRED, REQUIRED);
        } else if (brand.length() > 50) {
            errors.rejectValue("brand", INVALID, "brand name length should be less than 50");
        }

        // name validation
        String name = product.getName();
        if (!StringUtils.hasLength(name)) {
            errors.rejectValue("name", REQUIRED, REQUIRED);
        } else if (name.length() > 50) {
            errors.rejectValue("name", INVALID, "name length should be less than 50");
        }

        // description validation
        if (!StringUtils.hasLength(product.getDescription())) {
            errors.rejectValue("description", REQUIRED, REQUIRED);
        }

        // category validation
        Category category = product.getCategory();
        if (category == null || category.getName().equalsIgnoreCase("none")) {
            errors.rejectValue("category", REQUIRED, REQUIRED);
        }
    }
}
