package ss.funcmatic.demo2.entity.validator;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ss.funcmatic.demo2.entity.User;

@Component
public class UserValidator implements Validator {
    private static final String REQUIRED = "required";
    private static final String INVALID = "invalid";

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        User user = (User) target;

        // username validation
        String username = user.getUsername();
        if (!StringUtils.hasLength(username)) {
            errors.rejectValue("username", REQUIRED, REQUIRED);
        } else if (username.length() > 50) {
            errors.rejectValue("username", INVALID, "name length should be shorter than 50");
        }

        // password validation
        String password = user.getPassword();
        if (!StringUtils.hasLength(user.getPassword())) {
            errors.rejectValue("description", REQUIRED, REQUIRED);
        } else if (password.length() < 4) {
            errors.rejectValue("password", INVALID, "password length should be longer than 4");
        }
    }
}
