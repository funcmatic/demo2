package ss.funcmatic.demo2.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ss.funcmatic.demo2.entity.Category;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
    List<Category> findAll();
}
