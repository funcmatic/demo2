package ss.funcmatic.demo2.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ss.funcmatic.demo2.entity.Product;

import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Integer> {
    Optional<Product> findById(Integer id);
}
