package ss.funcmatic.demo2.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ss.funcmatic.demo2.entity.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByUsername(String username);
}
